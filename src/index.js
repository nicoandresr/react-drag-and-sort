import PropTypes from 'prop-types';
import React from 'react';

import 'twin.macro';

const propTypes = {
  name: PropTypes.string
};

function Index(props) {
  const { name } = props;
  return (
    <h1 tw="underline text-pink-500">Hola {name}, desde twin & emotion</h1>
  );
}

Index.propTypes = propTypes;

export default Index;
