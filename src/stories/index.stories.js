import React from "react";

import DragAndSort from "../";

export default {
  title: "Example/DragAndSort",
  component: DragAndSort,
};

const Template = (args) => <DragAndSort {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  name: "Nico",
};
